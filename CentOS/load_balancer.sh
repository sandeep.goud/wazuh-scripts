#!/bin/bash
# Install Elastic data node using Cloudformation template

touch /tmp/deploy.log

echo "Nginx Load Balancing: Starting process." > /tmp/deploy.log
ssh_username="wazuh_elastic"
ssh_password="2021"
eth0_ip=$(hostname -I |  head -1 | cut -d' ' -f1)
node_name=3
echo "Added env vars." >> /tmp/deploy.log
echo "eth0_ip: $eth0_ip" >> /tmp/deploy.log

check_root(){
    # Check if running as root
    if [[ $EUID -ne 0 ]]; then
        echo "NOT running as root. Exiting" >> /tmp/deploy.log
        echo "This script must be run as root"
        exit 1
    fi
    echo "Running as root." >> /tmp/deploy.log
}

create_ssh_user(){
    # Creating SSH user
    if ! id -u ${ssh_username} > /dev/null 2>&1; then adduser ${ssh_username}; fi
    echo "${ssh_username} ALL=(ALL)NOPASSWD:ALL" >> /etc/sudoers
    usermod --password $(openssl passwd -1 ${ssh_password}) ${ssh_username}
    echo "Created SSH user." >> /tmp/deploy.log

    sed -i 's|[#]*PasswordAuthentication no|PasswordAuthentication yes|g' /etc/ssh/sshd_config
    systemctl restart sshd
    echo "Started SSH service." >> /tmp/deploy.log
}

install_nginx(){
    
    #Install dependencies
    yum install curl unzip wget libcap -y
    yum install epel-release -y
    echo "Dependencies are installed." >> /tmp/deploy.log

    # Installing NGINX
    yum install nginx -y
    echo "Installed NGINX." >> /tmp/deploy.log
}

configuring_load_balancer(){
    echo "Adding wazuh instances to NGINX." >> /tmp/deploy.log

    cat >> /etc/nginx/nginxconf << EOF
stream {
    upstream master {
        server 192.168.1.189:1515;
    }
    upstream mycluster {
        server 192.168.1.189:1514;
        server 192.168.1.222:1514;
    }
    server {
	listen 1515;
        proxy_pass master;
    }
    server {
	listen 1514;
        proxy_pass mycluster;
    }
}

EOF

echo "Added wazuh instances to NGINX configuration file." >> /tmp/deploy.log

setsebool httpd_can_network_connect on # to allow communication between NGINX and WAZUH NODES

echo "Allow 1515 and 1514 port in nginx using semanage and firewall to listen agents alerts." >> /tmp/deploy.log

semanage port -a -t http_port_t  -p tcp 1515
semanage port -a -t http_port_t  -p tcp 1514

firewall-cmd --zone=public --permanent --add-port 1515/tcp
firewall-cmd --zone=public --permanent --add-port 1514/tcp


echo "NGINX configuration completed." >> /tmp/deploy.log

systemctl enable nginx
systemctl start nginx
echo "NGINX service started." >> /tmp/deploy.log

sed -i -e "s#SELINUX=enforcing#SELINUX=disabled#" /etc/sysconfig/selinux
echo "selinux is disabled." >> /tmp/deploy.log

systemctl restart nginx

}

main(){
    check_root
    create_ssh_user
    install_nginx
    configuring_load_balancer
}

main