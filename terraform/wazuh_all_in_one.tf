#Terraform Settings Block
terraform {
   required_providers {
     aws = {
       source = "hashicorp/aws"
       #version = "~> 3.21" # Optional step but recommended
     }
   }
}
# Provider Block
provider "aws" {
   profile = "default" # AWS Credentials Profile configured on your local desktop terminal  $HOME/.aws/credentials
   region = "ap-south-1" # check the region
}
locals {
   user_data = <<EOF
#!/bin/bash
  sudo curl -so ~/unattended-installation.sh https://packages.wazuh.com/resources/4.2/open-distro/unattended-installation/unattended-installation.sh && sudo bash ~/unattended-installation.sh -o
EOF
}
# Resource Block
   resource "aws_instance" "ec2rhymdemo3" {
   ami           = "ami-0a23ccb2cdd9286bb" # Amazon Linux in asia-2,update as per your region
   instance_type = "t2.large"              # free tier
   vpc_security_group_ids = ["sg-970b12ee"]
   subnet_id = "subnet-f2d1cf9a"
   associate_public_ip_address = true
   user_data_base64 = base64encode(local.user_data)
   availability_zone = "ap-south-1a"
   root_block_device  {
       volume_type = "gp2"
       volume_size = 100
       delete_on_termination = true
     }
}
