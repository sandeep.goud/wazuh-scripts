# Terraform Settings Block
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      #version = "~> 3.21" # Optional step but recommended
    }
    tls = {
      source  = "hashicorp/tls"
      #version = "~> 3.21" # Optional step but recommended
    }
  }
}

# Provider Block
provider "aws" {
  profile = "default" # AWS Credentials Profile configured on your local desktop terminal  $HOME/.a>
  region  = "ap-south-1"  # check the region
}

resource "tls_private_key" "random_key"{
  algorithm = "RSA"
  rsa_bits = 4096
}

resource "aws_key_pair" "generated_key" {
  public_key = tls_private_key.random_key.public_key_openssh
}

# Resource Block VPC
resource "aws_vpc" "ownvpc" {
  cidr_block       = "192.168.0.0/16"
  instance_tenancy = "default"
  enable_dns_hostnames = "true"
  enable_classiclink = "false"
  enable_dns_support = "true"
}

# Resource Block VPC Creating subnets
resource "aws_subnet" "public" {
  vpc_id     = aws_vpc.ownvpc.id
  cidr_block = "192.168.1.0/24"
  availability_zone = "ap-south-1a"
  map_public_ip_on_launch = "true"
}

# Resource Block Creating internet gateway
resource "aws_internet_gateway" "mygw" {
  vpc_id = aws_vpc.ownvpc.id
}

# Resource Block  Creating Route Table
resource "aws_route_table" "my_route_table1" {
  vpc_id = aws_vpc.ownvpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.mygw.id
  }
}

resource "aws_route_table_association" "route_table_association1" {
  subnet_id      = aws_subnet.public.id
  route_table_id = aws_route_table.my_route_table1.id
}


# Resource Block Creating Security Groups
resource "aws_security_group" "security_group" {
  description = "Security group for web-server with HTTP ports open within VPC"
  vpc_id      = aws_vpc.ownvpc.id
  #egress {
    #    from_port = 0
   #     to_port = 0
  #      protocol = -1
 #       cidr_blocks = ["0.0.0.0/0"]
#    }
  ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"        
        #This means, all ip address are allowed to ssh ! 
        # Do not do it in the production. 
        # Put your office or home address in it!
        cidr_blocks = ["0.0.0.0/0"]
    }
}

resource "aws_network_interface" "networkiface_elk" {
  subnet_id   = aws_subnet.public.id
  private_ips = ["192.168.1.163"]

  tags = {
    Name = "primary_network_interface"
  }
}

resource "aws_network_interface" "networkiface_master" {
  subnet_id   = aws_subnet.public.id
  private_ips = ["192.168.1.189"]

  tags = {
    Name = "primary_network_interface"
  }
}

resource "aws_network_interface" "networkiface_worker" {
  subnet_id   = aws_subnet.public.id
  private_ips = ["192.168.1.222"]

  tags = {
    Name = "primary_network_interface"
  }
}

resource "aws_network_interface" "networkiface_load_balancer" {
  subnet_id   = aws_subnet.public.id
  private_ips = ["192.168.1.223"]

  tags = {
    Name = "primary_network_interface"
  }
}

locals {
   user_data = <<EOF
#!/bin/bash
  sudo curl -so ~/elastic.sh https://raw.githubusercontent.com/Sivavamsiraju/AWS-wazuh/main/elastic.sh && sudo bash ~/elastic.sh -o
EOF
}

# Resource Block
resource "aws_instance" "ELKinstance" {
  ami           = "ami-04db49c0fb2215364" # Amazon Linux in Mumbai, update as per your region
  instance_type = "t2.large"  # free tier
  network_interface {
    network_interface_id = aws_network_interface.networkiface_elk.id
    device_index         = 0
  }
  key_name = aws_key_pair.generated_key.id
  user_data_base64 = base64encode(local.user_data)
  tags = {
    Name = "ElasticSearch & Kibana"
  }
}

# Resource Block
resource "aws_instance" "WazuhMasterinstance" {
  ami           = "ami-04db49c0fb2215364" # Amazon Linux in Mumbai, update as per your region
  instance_type = "t2.xlarge"  # free tier
  network_interface {
    network_interface_id = aws_network_interface.networkiface_master.id
    device_index         = 0
  }
  key_name = aws_key_pair.generated_key.id
  user_data = <<EOF
#!/bin/bash
  sudo curl -so ~/wazuh-master.sh https://raw.githubusercontent.com/Sivavamsiraju/AWS-wazuh/main/wazuh-master.sh && sudo bash ~/wazuh-master.sh -o
EOF
#   user_data_base64 = base64encode(local.user_data)
  tags = {
    Name = "Wazuh Master Node"
  }
}

# Resource Block
resource "aws_instance" "WazuhWorkerinstance" {
  ami           = "ami-04db49c0fb2215364" # Amazon Linux in Mumbai, update as per your region
  instance_type = "t2.xlarge"  # free tier
  network_interface {
    network_interface_id = aws_network_interface.networkiface_worker.id
    device_index         = 0
  }
  key_name = aws_key_pair.generated_key.id
  user_data = <<EOF
#!/bin/bash
  sudo curl -so ~/wazuh_worker.sh https://raw.githubusercontent.com/Sivavamsiraju/AWS-wazuh/main/wazuh_worker.sh && sudo bash ~/wazuh_worker.sh -o
EOF
#   user_data_base64 = base64encode(local.user_data)
  tags = {
    Name = "Wazuh Worker Node"
  }
}


# Resource Block
resource "aws_instance" "NGINXinstance" {
  ami           = "ami-04db49c0fb2215364" # Amazon Linux in Mumbai, update as per your region
  instance_type = "t2.medium"  # free tier
  network_interface {
    network_interface_id = aws_network_interface.networkiface_load_balancer.id
    device_index         = 0
  }
  key_name = aws_key_pair.generated_key.id
  user_data = <<EOF
#!/bin/bash
  sudo curl -so ~/nginx_load_balance.sh https://raw.githubusercontent.com/Sivavamsiraju/AWS-wazuh/main/nginx_load_balance.sh && sudo bash ~/nginx_load_balance.sh -o
EOF
#   user_data_base64 = base64encode(local.user_data)
  tags = {
    Name = "NGINX"
  }
}

resource "aws_network_interface_sg_attachment" "sg_attachment1" {
  security_group_id    = aws_security_group.security_group.id
  network_interface_id = aws_instance.ELKinstance.primary_network_interface_id
}

resource "aws_network_interface_sg_attachment" "sg_attachment2" {
  security_group_id    = aws_security_group.security_group.id
  network_interface_id = aws_instance.WazuhMasterinstance.primary_network_interface_id
}

resource "aws_network_interface_sg_attachment" "sg_attachment3" {
  security_group_id    = aws_security_group.security_group.id
  network_interface_id = aws_instance.WazuhWorkerinstance.primary_network_interface_id
}
resource "aws_network_interface_sg_attachment" "sg_attachment4" {
  security_group_id    = aws_security_group.security_group.id
  network_interface_id = aws_instance.NGINXinstance.primary_network_interface_id
}

resource "aws_eip" "lb" {
  instance = aws_instance.NGINXinstance.id
  vpc      = true
}